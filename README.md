# PoL Generation

The PoL Generation library implements the Prover and Witness functionalities of the Proof of Location Generation protocol. 

## Requirements

The library requires iOS 7.0 and Xcode 10.2+ or higher to run. 

The following entries must be specified in the Info.plist file in order to use both the Prover and Witness functionalities:

- `NSBluetoothAlwaysUsageDescription`

- `NSBluetoothPeripheralUsageDescription`


If running on iOS 13.0 or higher, `NSBluetoothAlwaysUsageDescription`is sufficient.

## Dependencies

The library requires [SwiftyRSA](https://github.com/TakeScoop/SwiftyRSA) to be installed in order to perform encryption and signature. 

SwiftyRSA can be installed through [Cocoapods](https://cocoapods.org/)  or [Carthage](https://github.com/Carthage/Carthage).

## Usage - Prover
The following code allows to start a new Prover and receive Proofs of Location for a given Proof of Location ID.

`let proverID = DSGPeerID(string: "ExampleProversId", encoding: .utf8)` 

`let polID = DSGProofOfLocationID(string: "ProofOfLocationID123", encoding: .utf8)`

`let location = DSGLocation(latitude: 0.0, longitude: 0.0)`

`let prover = DSGProver(proverID: id, queue: nil, delegate: self)`

`prover?.setSigningKey(privateKey: privateKey)`

`prover?.startRequesting(proofOfLocation: polID, at: location)`


In the previous snippet, the peer:

- creates a `DSGPeerID` representing its idendity.

- creates a `DSGProofOfLocationID` representing the ID of the Proof of Location he wants to collect.

- creates a `DSGLocation`, representing its current coordinates.

- creates the `DSGProver` instance that will collect the Proofs of Location. The Prover requires a delegate that will receive updates about events occurring during the execution of the protocol.

- sets its RSA private signing key (512 bit), base64 encoded.

- starts the Prover, that will receive Proof of Locations from the nearby witnesses. 


The delegate of the `DSGProver` must implement the `ProverDelegate` protocol.


## Usage - Witness

The following code allows to start a new Witness and provide Proofs of Location for a given list of Proof of Location IDs.

`let witnessID = DSGPeerID(string: "ExampleWitnessId", encoding: .utf8)`

`let polID1 = DSGProofOfLocationID(string: "ProofOfLocationID123", encoding: .utf8)`

`let polID2 = DSGProofOfLocationID(string: "ProofOfLocationID456", encoding: .utf8)`

`let location = DSGLocation(latitude: 0.0, longitude: 0.0)`

`let witness = DSGWitness(witnessID: witnessID, queue: nil, delegate: self)`

`witness?.setSigningKey(privateKey: privateKey)`

`witness?.startWitnessing(for: [polID1,polID2], at: location, timestamp: timestamp)`


In the previous snippet, the peer:

- creates a `DSGPeerID` representing its idendity.

- creates two `DSGProofOfLocationID` representing the IDs of the Proofs of Location he can provide.

- creates a `DSGLocation`, representing its current coordinates.

- creates the `DSGWitness` instance that provide the Proofs of Location to the Provers. The Witness requires a delegate that will receive updates about events occurring during the execution of the protocol.

- sets its RSA private signing key (512 bit), base64 encoded.

- starts the Witness, that will provide Proofs of Locations to the nearby Provers. 


The delegate of the `DSGWitness` must implement the `DSGWitnessDelegate` protocol.