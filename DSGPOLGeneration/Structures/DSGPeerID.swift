//
//  DSGPeerID.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 12/06/21.
//

import Foundation

///
/// A class that represents the ID of peer in the Proof Of Location Exchange protocol
///
class DSGPeerID {

    private var idBytes: [UInt8]

    ///
    /// Class constructor.
    ///
    /// - Parameter data: the peer's id in a byte format (Array of UInt8)
    ///
    init?(bytes: [UInt8]) {
        var finalBytes = bytes

        guard bytes.count <= DSGProofOfLocationExchangeConfiguration.peerIDSize else {
            return nil
        }

        //Adds the missing bytes
        for _ in (0..<(Int(DSGProofOfLocationExchangeConfiguration.peerIDSize) - bytes.count)) {
            finalBytes.append(0)
        }
        self.idBytes = finalBytes
    }

    ///
    /// Class constructor.
    ///
    /// - Parameter data: the peer's id in a data format
    ///
    init?(data: Data) {
        let bytes = [UInt8](data)
        guard bytes.count == DSGProofOfLocationExchangeConfiguration.peerIDSize else {
            return nil
        }
        self.idBytes = bytes
    }

    ///
    /// Class constructor.
    ///
    /// - Parameter string: the string representing the peer's ID
    ///
    convenience init?(string: String, encoding: String.Encoding) {
        self.init(bytes: [UInt8](string.data(using: encoding)!))
    }


    ///
    /// Gets the bytes representing the peer's id.
    ///
    var bytes: [UInt8] {
        return idBytes
    }

    ///
    /// Gets the data representing the peer's id.
    ///
    var data: Data {
        return Data(idBytes)
    }

    ///
    /// Gets the string representing the peer's id.
    ///
    /// - Parameter encoding: the encoding of the string
    ///
    func toString(encoding: String.Encoding) -> String? {
        return String(data: Data(idBytes), encoding: encoding)
    }

}

