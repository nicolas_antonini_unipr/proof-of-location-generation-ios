//
//  DSGProofOfLocation.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

///
/// A class that represents a proof of location.
///
class DSGProofOfLocation {
    
    ///
    /// The size of the proof of location, in bytes.
    ///
    static let size: UInt32 = DSGProofOfLocationExchangeConfiguration.stringsSize * 3 + DSGMessageE.size + DSGProofOfLocationExchangeConfiguration.peerIDSize + DSGLocation.size + 8
    
    private var proofOfLocationBytes: [UInt8]
    
    ///
    /// Class constructor.
    ///
    /// - Parameter r: The string R sent from the prover to the witness
    /// - Parameter c: The string C sent from the witness to the prover
    /// - Parameter h: The string H
    /// - Parameter e: The message E sent from the prover to the witness
    /// - Parameter witnessID: The ID of the witness
    /// - Parameter location: The location of the witness
    /// - Parameter timestamp: A timestamp
    ///
    init(r: DSGStringR,
         c: DSGStringC,
         h: DSGStringH,
         e: DSGMessageE,
         witnessID: DSGPeerID,
         location: DSGLocation,
         timestamp: Double) {
        self.proofOfLocationBytes = []
        self.proofOfLocationBytes.append(contentsOf: r.bytes)
        self.proofOfLocationBytes.append(contentsOf: c.bytes)
        self.proofOfLocationBytes.append(contentsOf: h.bytes)
        self.proofOfLocationBytes.append(contentsOf: witnessID.bytes)
        self.proofOfLocationBytes.append(contentsOf: location.bytes)
        var mutableTimestamp = timestamp
        let timestampData = Data(bytes: &mutableTimestamp, count: MemoryLayout.size(ofValue: mutableTimestamp))
        self.proofOfLocationBytes.append(contentsOf: [UInt8](timestampData))
        self.proofOfLocationBytes.append(contentsOf: [0x43, 0x43, 0x43])
        self.proofOfLocationBytes.append(contentsOf: e.bytes)
        self.proofOfLocationBytes.append(contentsOf: [0x43, 0x43, 0x43])
    }
    
    ///
    /// Class constructor.
    ///
    /// - Parameter bytes: The bytes representing the proof of location
    ///
    init(bytes: [UInt8]) {
        self.proofOfLocationBytes = bytes
    }
    
    ///
    /// An array of UInt8 representing the proof of location
    ///
    var bytes: [UInt8] {
        return proofOfLocationBytes
    }
    
}
