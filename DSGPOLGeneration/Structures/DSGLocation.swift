//
//  DSGLocation.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

///
/// Represents the coordinates of a peer.
///
class DSGLocation {
    
    ///
    /// The size of the location, in bytes.
    ///
    static let size: UInt32 = (UInt32(MemoryLayout.size(ofValue: Double())) * 2)
    
    ///
    /// The latitude of the peer.
    ///
    var latitude: Double
    
    ///
    /// The longitude of the peer.
    ///
    var longitude: Double
    
    
    ///
    /// Class constructor.
    ///
    ///  - Parameter latitude: The latitude of the peer
    ///  - Parameter longitude: The longitude of the peer
    ///
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    ///
    /// An array of UInt8 representing the location.
    ///
    var bytes: [UInt8] {
        var locationBytes: [UInt8] = []
        let latitudeData = Data(bytes: &latitude, count: MemoryLayout.size(ofValue: latitude))
        let longitudeData = Data(bytes: &longitude, count: MemoryLayout.size(ofValue: longitude))
        locationBytes.append(contentsOf: [UInt8](latitudeData))
        locationBytes.append(contentsOf: [UInt8](longitudeData))
        return locationBytes
    }
}
