//
//  DSGProofOfLocationID.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

///
/// A class that represents the ID of a proof of location.
///
///    The DSGProofOfLocationID class defines the methods to construct and manage proof of locations IDs.
///
class DSGProofOfLocationID: Equatable {

    private var idBytes: [UInt8]
    
    ///
    /// Class constructor.
    ///
    /// - Parameter data: the proof of location id in a byte format (Array of UInt8)
    ///
    init?(bytes: [UInt8]) {
        var finalBytes = bytes
        
        guard bytes.count <= DSGProofOfLocationExchangeConfiguration.proofOfLocationIDSize else {
            return nil
        }
        
        //Adds the missing bytes
        for _ in (0..<(Int(DSGProofOfLocationExchangeConfiguration.proofOfLocationIDSize) - bytes.count)) {
            finalBytes.append(0)
        }
        self.idBytes = finalBytes
    }
    
    ///
    /// Class constructor.
    ///
    /// - Parameter data: the proof of location id in a data format
    ///
    init?(data: Data) {
        let bytes = [UInt8](data)
        guard bytes.count == DSGProofOfLocationExchangeConfiguration.proofOfLocationIDSize else {
            return nil
        }
        self.idBytes = bytes
    }
    
    ///
    /// Class constructor.
    ///
    /// - Parameter string: the proof of location id in a string format
    /// - Parameter encoding: the encoding of the string
    ///
    convenience init?(string: String, encoding: String.Encoding) {
        let stringData = string.data(using: encoding)
        if let stringData = stringData {
            self.init(bytes: [UInt8](stringData))
        } else {
            return nil
        }
    }
    
    ///
    /// Gets the bytes representing the proof of location id.
    ///
    var bytes: [UInt8] {
        return idBytes
    }
    
    ///
    /// Gets the data representing the proof of location id.
    ///
    var data: Data {
        return Data(idBytes)
    }
    
    ///
    /// Gets the string representing the proof of location id.
    ///
    /// - Parameter encoding: the encoding of the string
    ///
    func toString(encoding: String.Encoding) -> String? {
        return String(data: Data(idBytes), encoding: encoding)
    }
    
    static func == (lhs: DSGProofOfLocationID, rhs: DSGProofOfLocationID) -> Bool {
        var equals = true
        guard lhs.bytes.count == rhs.bytes.count else {
            return false
        }
        for i in 0..<lhs.bytes.count {
            if lhs.bytes[i] != rhs.bytes[i] {
                equals = false
            }
        }
        return equals
    }
    
}
