//
//  DSGProverStatus.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 12/06/21.
//

import Foundation

///
/// Represent the possible status of a DSGProver object
///
enum DSGProverStatus: String {
    
    ///
    /// In this status, the GATT server is not advertising connectable packets and it's ready to start
    ///
    case idle = "Idle"
    
    ///
    /// In this status, a witness has read the POL ID and the prover is sending the message E
    ///
    case connected = "Connected"
    
    ///
    /// In this status, the prover is performing the distance bounding protocol
    ///
    case distanceBounding = "DistanceBounding"
    
    ///
    /// In this status, the prover is waiting for the result of the distance bounding protocol
    ///
    case waitingResults = "WaitingResults"
    
    ///
    /// In this status, the prover is waiting for the proof of location
    ///
    case receiving = "Receiving"
}
