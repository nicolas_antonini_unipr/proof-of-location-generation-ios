//
//  DSGProver.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 12/06/21.
//

import Foundation
import CoreBluetooth
import SwiftyRSA
import CommonCrypto

///
/// A class that provides the Prover functionalities in the Proof Of Location Exchange Protocol.
///
/// The class DSGProver allows to search for witness for a provided proof of location id. The
/// objects of this class will call methods on their delegates to provide the results.
///
class DSGProver: NSObject {
    
    ///
    /// The current status of the prover
    ///
    private var status: DSGProverStatus = .idle
    
    ///
    /// The object on which the delegate methods will be called
    ///
    private weak var delegate: DSGProverDelegate?
    
    ///
    /// The BLE peripheral manager, used to manage incoming connections from witnesses (BLE centrals)
    ///
    private var peripheralManager: CBPeripheralManager?
    
    ///
    /// The dispatch queue for dispatching the peripheral role events
    ///
    private var dispatchQueue: DispatchQueue?
    
    ///
    /// The ID of the prover
    ///
    private var proverID: DSGPeerID
    
    ///
    /// The string A
    ///
    private var stringA: DSGStringA?
    
    ///
    /// The string B
    ///
    private var stringB: DSGStringB?
    
    ///
    /// The string Z
    ///
    private var stringZ: DSGStringZ?
    
    ///
    /// The distance bounding iteration index
    ///
    private var distanceBoundingIterationIndex: Int = 0
    
    ///
    /// The current messageE chunk sent
    ///
    private var messageEChunkIndex: Int = 0
    
    ///
    /// The ID of the proof of location to be retrieved
    ///
    private var proofOfLocationID: DSGProofOfLocationID?
    
    ///
    /// The message E
    ///
    private var messageE: DSGMessageE?
    
    ///
    /// The location of the prover
    ///
    private var location: DSGLocation?
    
    ///
    /// The BLE characteristic that contains the Proof of location ID
    ///
    private var bleCharacteristicID: CBMutableCharacteristic?

    ///
    /// The BLE characteristic used to receive data from the witness
    ///
    private var bleCharacteristicDataRx: CBMutableCharacteristic?

    ///
    /// The BLE characteristic used to send data to the witness
    ///
    private var bleCharacteristicDataTx: CBMutableCharacteristic?

    ///
    /// The BLE characteristic used to send responses (R packets) to the witness for the distance bounding protocol
    ///
    private var bleCharacteristicDBTx: CBMutableCharacteristic?

    ///
    /// The BLE characteristic used to receive challenges (C packets) from the witness for the distance bounding protocol
    ///
    private var bleCharacteristicDBRx: CBMutableCharacteristic?
    
    ///
    /// The BLE service that provides the proof of location exchange functionalities
    ///
    private var bleService: CBMutableService?
    
    ///
    /// The buffer containing chunks of a proof of location
    ///
    private var proofOfLocationBuffer: [UInt8] = []
    
    ///
    /// The private signing key
    ///
    private var signingKey: String!
    
    ///
    /// The buffer that contains the message E that has to be sent
    ///
    private var encryptedMessageEBuffer: [UInt8] = []
    
    ///
    /// The size of the received proof of location
    ///
    private var proofOfLocationSize: Int? = nil
    
    ///
    /// Class constructor.
    ///
    /// - parameter proverID: The peer's ID of the prover
    /// - parameter queue: The dispatch queue for dispatching the BLE peripheral role events
    /// - parameter delegate: The object on which the delegate methods will be called
    ///
    init(proverID: DSGPeerID,
         queue: DispatchQueue?,
         delegate: DSGProverDelegate) {
        print("Prover initialized")
        self.proverID = proverID
        self.dispatchQueue = queue
        self.delegate = delegate
    }
    
    ///
    /// Sets the private signing key of the prover
    ///
    func setSigningKey(privateKey: String) {
        self.signingKey = privateKey
    }
    
    ///
    /// Begins to search proof of locations for a given ID
    ///
    /// - parameter proofOfLocation: The id of the proof of location
    /// - parameter at: The location of the prover
    ///
    func startRequesting(proofOfLocation proofOfLocationID: DSGProofOfLocationID,
                         at location: DSGLocation) {
        print("Prover status: \(self.status.rawValue) - startRequesting")
        self.proofOfLocationID = proofOfLocationID
        self.location = location
        self.peripheralManager = CBPeripheralManager(delegate: self,
                                                     queue: self.dispatchQueue)
    }
    
    ///
    /// Configures the GATT server
    ///
    func setupPeripheral() {
        print("Prover status: \(self.status.rawValue) - setupPeripheral")
        self.bleCharacteristicID =
            CBMutableCharacteristic(type: DSGBLEService.POLIDCharacteristicUUID,
                                    properties: [.read],
                                    value: self.proofOfLocationID?.data,
                                   permissions: [.readable])
        self.bleCharacteristicDBRx =
            CBMutableCharacteristic(type: DSGBLEService.DBRxCharacteristicUUID,
                                    properties: [.writeWithoutResponse],
                                   value: nil,
                                   permissions: [.writeable])
        self.bleCharacteristicDBTx =
            CBMutableCharacteristic(type: DSGBLEService.DBTxCharacteristicUUID,
                                    properties: [.notify],
                                   value: nil,
                                   permissions: [.readable])
        self.bleCharacteristicDataRx =
            CBMutableCharacteristic(type: DSGBLEService.DataRxCharacteristicUUID,
                                    properties: [.write, .writeWithoutResponse],
                                   value: nil,
                                   permissions: [.writeable])
        self.bleCharacteristicDataTx =
            CBMutableCharacteristic(type: DSGBLEService.DataTxCharacteristicUUID,
                                    properties: [.notify, .read],
                                   value: nil,
                                   permissions: [.readable])
        
        self.bleService = CBMutableService(type: DSGBLEService.POLServiceUUID, primary: true)
        self.bleService!.characteristics = [self.bleCharacteristicID!,
                                            self.bleCharacteristicDBRx!,
                                            self.bleCharacteristicDBTx!,
                                            self.bleCharacteristicDataRx!,
                                            self.bleCharacteristicDataTx! ]
        
        self.peripheralManager!.add(self.bleService!)
        self.peripheralManager?.startAdvertising(nil)

    }
    
}

//MARK: - CBPeripheralManagerDelegate

extension DSGProver: CBPeripheralManagerDelegate {
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print("Prover status: \(self.status.rawValue) - peripheralManagerDidUpdateState")
        switch peripheral.state {
        //In this case, the BLE is turned on and available
        case .poweredOn:
            self.setupPeripheral()
        //In every other case, the BLE is not available
        case .unknown:
            self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                  proofOfLocation: self.proofOfLocationID!)
            self.peripheralManager = nil
            self.status = .idle
        case .resetting:
            self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                  proofOfLocation: self.proofOfLocationID!)
            self.peripheralManager = nil
            self.status = .idle
        case .unsupported:
            self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                  proofOfLocation: self.proofOfLocationID!)
            self.peripheralManager = nil
            self.status = .idle
        case .unauthorized:
            self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                  proofOfLocation: self.proofOfLocationID!)
            self.peripheralManager = nil
            self.status = .idle
        case .poweredOff:
            self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                  proofOfLocation: self.proofOfLocationID!)
            self.peripheralManager = nil
            self.status = .idle
        @unknown default:
            fatalError()
        }
    }
    
    ///
    /// A BLE central subscribed to a BLE characteristic
    ///
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
        print("Prover status: \(self.status.rawValue) - peripheralManager:didSubscribeTo")
        if (characteristic == self.bleCharacteristicDataTx) {
            self.status = .connected
            self.stringA = DSGStringA()
            self.stringB = DSGStringB()
            self.messageE = DSGMessageE(a: self.stringA!,
                                       b: self.stringB!,
                                       id: self.proverID,
                                       location: self.location!)
            
            print(messageE?.bytes.count)
            
            messageEChunkIndex = 0
            
            let pubk = try! PublicKey(base64Encoded: DSGProofOfLocationExchangeConfiguration.verifierPublicKey)
            let pvtk = try! PrivateKey(base64Encoded: self.signingKey)
            let clear = ClearMessage(data: Data(self.messageE!.bytes))
            
            let signature = try! clear.signed(with: pvtk, digestType: .sha256)
            let signatureData = signature.data
            
            var signedMessageE = messageE!.bytes
            signedMessageE.append(contentsOf: [UInt8](signatureData))
            
            let aes128KeyArr = (1...16).map( {_ in UInt8.random(in: 0...255)} )
            
            let encryptedData = DSGEncryption.aesEncryption(data:Data(signedMessageE),
                                             keyData: Data(aes128KeyArr),
                                             operation:kCCEncrypt)
            
            let clearKey = ClearMessage(data: Data(aes128KeyArr))
            
            let encryptedKey = try! clearKey.encrypted(with: pubk, padding: .PKCS1)
            
            var mergedData = [UInt8](encryptedData)
            mergedData.append(contentsOf: [0x2A, 0x2A, 0x2A])
            mergedData.append(contentsOf: [UInt8](encryptedKey.data))
            mergedData.append(contentsOf: [0x2A, 0x2A, 0x2A])
            
            
//            let clearSigned = ClearMessage(data: Data(signedMessageE))
//            let encryptedMessageE = try! clearSigned.encrypted(with: pubk, padding: .PKCS1)
//            let encryptedMessageEData = encryptedMessageE.data
            
            var encryptedMessageEBytesWithLenght = [UInt8(mergedData.count / 256),
                                                    UInt8(mergedData.count % 256)]
            
            encryptedMessageEBytesWithLenght.append(contentsOf: [UInt8](mergedData))
            
            self.encryptedMessageEBuffer = encryptedMessageEBytesWithLenght
            
            var sent = true
            while (sent) {
                let bytes = encryptedMessageEBytesWithLenght
                let count = bytes.count
                let chunkSize = Int(DSGProofOfLocationExchangeConfiguration.maxBLEPacketPayloadSize)
                let numberOfChunks = ( Double(count) / Double(chunkSize) )
                let numberOfChunksRoundedUp = Int(numberOfChunks.rounded(.up))
                if (messageEChunkIndex == (numberOfChunksRoundedUp - 1)) {
                    break
                } else {
                    let lastIndex = min(count - 1, messageEChunkIndex * chunkSize + (chunkSize-1) )
                    let startIndex = messageEChunkIndex * chunkSize
                    let subBytes = Array(bytes[startIndex...lastIndex])
                    let subData = Data(subBytes)
                    sent = peripheralManager!.updateValue(subData, for: self.bleCharacteristicDataTx!, onSubscribedCentrals: nil)
                    if sent {
                        messageEChunkIndex += 1
                    }
                }
            }
            
        } else if (characteristic == self.bleCharacteristicDBTx) {
            self.status = .distanceBounding
            self.distanceBoundingIterationIndex = 0
        }
    }
    
    ///
    /// A BLE central unsubscribed from a BLE characteristic
    ///
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFrom characteristic: CBCharacteristic) {
        print("Prover status: \(self.status.rawValue) - peripheralManager:didUnsubscribeFrom")
        //TODO: Handle cases
        self.status = .idle
        self.peripheralManager?.stopAdvertising()
    }
    
    ///
    /// A write request has failed and now it can be performed
    ///
    func peripheralManagerIsReady(toUpdateSubscribers peripheral: CBPeripheralManager) {
        print("Prover status: \(self.status.rawValue) - peripheralManagerIsReady:")
        var sent = true
        while (sent) {
            let bytes = self.encryptedMessageEBuffer
            let count = bytes.count
            let chunkSize = Int(DSGProofOfLocationExchangeConfiguration.maxBLEPacketPayloadSize)
            let numberOfChunks = ( Double(count) / Double(chunkSize) )
            let numberOfChunksRoundedUp = Int(numberOfChunks.rounded(.up))
            if (messageEChunkIndex == (numberOfChunksRoundedUp)) {
                break
            } else {
                let lastIndex = min(count - 1, messageEChunkIndex * chunkSize + (chunkSize-1) )
                let startIndex = messageEChunkIndex * chunkSize
                let subBytes = Array(bytes[startIndex...lastIndex])
                let subData = Data(subBytes)
                sent = peripheralManager!.updateValue(subData, for: self.bleCharacteristicDataTx!, onSubscribedCentrals: nil)
                if sent {
                    messageEChunkIndex += 1
                }
            }
        }
    }
    
    ///
    /// A write request has been received
    ///
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        print("Prover status: \(self.status.rawValue) - peripheralManager:didReceiveWrite")
        if (self.status == .connected) {
            guard let value = requests.first?.value else {
                self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.inconsistency,
                                      proofOfLocation: self.proofOfLocationID!)
                return
            }
            guard let stringH = DSGStringH(data: value) else  {
                self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.message,
                                      proofOfLocation: self.proofOfLocationID!)
                return
            }
            
            self.stringZ = DSGStringZ(b: self.stringB!, h: stringH)
            peripheralManager?.respond(to: requests.first!, withResult: .success)
        } else if (status == .distanceBounding) {
            guard let value = requests.first?.value else {
                self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.inconsistency,
                                      proofOfLocation: self.proofOfLocationID!)
                return
            }
            
            let byte = [UInt8](value).first!
            let i = self.distanceBoundingIterationIndex
            let responseByte = (self.stringA!.bytes[i] & (byte ^ 0xFF)) | (self.stringZ!.bytes[i] & byte)
            peripheralManager?.updateValue(Data([responseByte]), for: self.bleCharacteristicDBTx!, onSubscribedCentrals: nil)
            
            if (i == (DSGStringC.size - 1)) {
                self.status = .waitingResults
            }
            self.distanceBoundingIterationIndex += 1
        } else if (status == .waitingResults) {
            guard let value = requests.first?.value else {
                self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.message,
                                      proofOfLocation: self.proofOfLocationID!)
                return
            }
            
            let result = DSGProofOfLocationResult(data: value)
            
            if !result.isValid {
                self.status = .idle
                self.peripheralManager?.stopAdvertising()
                self.delegate?.prover(witnessDidDiscard: self.proofOfLocationID!)
                
            } else {
                self.status = .idle
                self.peripheralManager?.stopAdvertising()
                self.proofOfLocationBuffer = []
                status = .receiving
            }
        } else if (status == .receiving) {
            guard let value = requests.first?.value else {
                self.delegate?.prover(didFailWith: DSGProofOfLocationExchangerError.BLE.message,
                                      proofOfLocation: self.proofOfLocationID!)
                return
            }
            
            let bytes = [UInt8](value)
            if self.proofOfLocationSize == nil {
                self.proofOfLocationSize = Int(bytes[0]) * 256 + Int(bytes[1])
                proofOfLocationBuffer.append(contentsOf: bytes[2...])
            } else {
                proofOfLocationBuffer.append(contentsOf: bytes)
            }
            
            print("if \(proofOfLocationBuffer.count) == \(self.proofOfLocationSize!)")
            if proofOfLocationBuffer.count == self.proofOfLocationSize! {
                let proofOfLocation = DSGProofOfLocation(bytes: proofOfLocationBuffer)
                delegate?.prover(didReceive: proofOfLocation, proofOfLocation: self.proofOfLocationID!)
                self.peripheralManager?.stopAdvertising()
                status = .idle
            }
        }
    }
        
}
