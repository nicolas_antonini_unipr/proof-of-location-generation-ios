//
//  DSGPOLExchangeProverDelegate.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

///
/// A protocol that provides updates for the discovery and management of witnesses.
///
///    The DSGProverDelegate protocol defines the methods that a delegate of a DSGProver object must adopt.
///    The mandatory methods allows to be notified when a proof of location is confirmed or rejected.
///
protocol DSGProverDelegate: AnyObject {
    
    ///
    /// Tells the delegate that the prover failed to retrieve a proof of location
    ///
    ///    The prover calls this method after that the method startProving() has been called
    ///    if the proof of location cannot be retrieved.
    ///
    /// - Parameter error: The error that caused the prover to stop
    /// - Parameter proofOfLocation: The id of the proof of location that failed
    ///
    func prover(didFailWith error: Error,
                proofOfLocation proofOfLocationID: DSGProofOfLocationID)
    
    ///
    /// Tells the delegate that the prover has received a proof of location
    ///
    ///    The prover calls this method after that the method startProving() has been called
    ///    if a proof of location has been received successfully.
    ///
    /// - Parameter didReceive: The proof of location received
    /// - Parameter proofOfLocation: The id of the received Proof of Location.
    ///
    func prover(didReceive proofOfLocation: DSGProofOfLocation,
                proofOfLocation proofOfLocationID: DSGProofOfLocationID)
    
    ///
    /// Tells the delegate that the witness has rejected a proof of location
    ///
    ///    The prover calls this method after that the method startProving() has been called
    ///    if a proof of location has been rejected by the witness.
    ///
    /// - Parameter didSend: The proof of location just sent
    /// - Parameter proofOfLocation: The id of the proof of location that has been discarded
    ///
    func prover(witnessDidDiscard proofOfLocationID: DSGProofOfLocationID)
}
