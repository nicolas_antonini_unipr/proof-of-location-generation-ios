//
//  DSGPOLExchangerWitnessDelegate.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

///
/// A protocol that provides updates for the discovery and management of provers.
///
///    The DSGWitnessDelegate protocol defines the methods that a delegate of a DSGWitness object must adopt.
///    The mandatory methods allows to be notified when a proof of location is confirmed or rejected.
///
protocol DSGWitnessDelegate: AnyObject {
    
    ///
    /// Tells the delegate that the witness failed to provide a proof of location
    ///
    ///    The witness calls this method after that the method startWitnessing() has been called
    ///    if the proof of location cannot be generated.
    ///
    /// - Parameter error: The error that caused the witness to stop
    /// - Parameter proofOfLocationIDs: The IDs of the proof of locations that failed
    ///
    func witness(didFailWith error: DSGError,
                 proofOfLocationIDs: [DSGProofOfLocationID])
    
    ///
    /// Tells the delegate that the witness has sent a proof of location
    ///
    ///    The witness calls this method after that the method startWitnessing() has been called
    ///    if a proof of location has been sent successfully.
    ///
    /// - Parameter didSend: The proof of location just sent
    /// - Parameter proofOfLocation: The id of the proof of location that has been sent
    ///
    func witness(didSend proofOfLocation: DSGProofOfLocation,
                 proofOfLocation proofOfLocationID: DSGProofOfLocationID)
    
    ///
    /// Tells the delegate that the witness has rejected a proof of location
    ///
    ///    The witness calls this method after that the method startWitnessing() has been called
    ///    if a proof of location has been rejected.
    ///
    /// - Parameter didDiscard: The proof of location just discarded
    /// - Parameter proofOfLocation: The id of the proof of location that has been discarted
    ///
    func witness(didDiscard proofOfLocationID: DSGProofOfLocationID)
}
