//
//  DSGWitness.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 12/06/21.
//

import Foundation
import CoreBluetooth
import SwiftyRSA
import CommonCrypto

///
/// A class that provides the Witness functionalities in the Proof Of Location Exchange Protocol.
///
/// The class DSGWitness allows to provide proofs of location for a certain proof of location id. The
/// objects of this class will call methods on their delegates to provide the results.
///
class DSGWitness: NSObject {

    ///
    /// The current status of the witness
    ///
    private var status: DSGWitnessStatus = .idle

    ///
    /// The object on which the delegate methods will be called
    ///
    private weak var delegate: DSGWitnessDelegate?

    ///
    /// The BLE central manager, used to connect and communicate to the provers (BLE peripherals)
    ///
    private var centralManager: CBCentralManager?

    ///
    /// The dispatch queue for dispatching the central role events
    ///
    private var dispatchQueue: DispatchQueue?

    ///
    /// The ID of the witness
    ///
    private var witnessID: DSGPeerID

    ///
    /// The Timer that stops the scannin of BLE peripherals
    ///
    private var bleScanTimeoutTimer: Timer?

    ///
    /// The Timer that stops an attempt to connect to a BLE peripheral
    ///
    private var bleConnectionAttemptTimeoutTimer: Timer?

    ///
    /// The BLE peripherals found
    ///
    private var blePeripherals: [CBPeripheral]?

    ///
    /// The currently connected BLE peripheral
    ///
    private var connectedBLEPeripheral: CBPeripheral?

    ///
    /// The index of the currently connected BLE peripheral
    ///
    private var blePeripheralsIndex: Int = 0

    ///
    /// The BLE characteristic that contains the Proof of location ID
    ///
    private var bleCharacteristicID: CBCharacteristic?

    ///
    /// The BLE characteristic used to send data to the prover
    ///
    private var bleCharacteristicDataRx: CBCharacteristic?

    ///
    /// The BLE characteristic used to receive data from the prover
    ///
    private var bleCharacteristicDataTx: CBCharacteristic?

    ///
    /// The BLE characteristic used to receive responses (R packets) from the prover for the distance bounding protocol
    ///
    private var bleCharacteristicDBTx: CBCharacteristic?

    ///
    /// The BLE characteristic used to send challenges (C packets) to the prover for the distance bounding protocol
    ///
    private var bleCharacteristicDBRx: CBCharacteristic?

    ///
    /// The list of proof of location id's that the witness can serve
    ///
    private var proofOfLocationIDs: [DSGProofOfLocationID]?
    
    ///
    /// The ID of the currently served proof of location
    ///
    private var proofOfLocationID: DSGProofOfLocationID?

    ///
    /// The current position of the witness
    ///
    private var location: DSGLocation?

    ///
    /// The current timestamp
    ///
    private var timestamp: Double?

    ///
    /// The error that caused the disconnection
    ///
    private var error: DSGError?
    
    ///
    /// Buffer that contains chunks of message E
    ///
    private var messageEBuffer: [UInt8]?
    
    ///
    /// The received message E
    ///
    private var messageE: DSGMessageE?
    
    ///
    /// The string H
    ///
    private var stringH: DSGStringH?
    
    ///
    /// The string C
    ///
    private var stringC: DSGStringC?
    
    ///
    /// The string R
    ///
    private var stringR: DSGStringR?
    
    ///
    /// An array of timestamps corresponding to the sending of a distance bounding packet
    ///
    private var distanceBoundingStartTimestamps: [DispatchTime]?
    
    ///
    /// An array of timestamps corresponding to the arrival of the response to a distance bounding packet
    ///
    private var distanceBoundingEndTimestamps: [DispatchTime]?
    
    ///
    /// The index of the current distance bounding iteration
    ///
    private var distanceBoundingIterationIndex: Int = 0
    
    ///
    /// The private signing key
    ///
    private var signingKey: String!
    
    ///
    ///
    ///
    private var messageEncryptedSize: Int? = nil

    ///
    /// Class constructor.
    ///
    /// - parameter witnessID: The peer's ID of the witness
    /// - parameter queue: The dispatch queue for dispatching the BLE central role events
    /// - parameter delegate: The object on which the delegate methods will be called
    ///
    init(witnessID: DSGPeerID,
         queue: DispatchQueue?,
         delegate: DSGWitnessDelegate) {
        print("Witness Initialized")
        self.witnessID = witnessID
        self.dispatchQueue = queue
        self.delegate = delegate
    }
    
    ///
    /// Sets the private signing key of the witness
    ///
    func setSigningKey(privateKey: String) {
        self.signingKey = privateKey
    }

    ///
    /// Begins to search for provers that need for a proof of location.
    ///
    /// - parameter for: The list of proof of location IDs that can be served
    /// - parameter at: The current position of the witness
    /// - parameter timestamp: The timestamp of the current time
    ///
    /// - returns: true if successful, false if busy
    ///
    @discardableResult
    func startWitnessing(for proofOfLocationIDs: [DSGProofOfLocationID],
                         at location: DSGLocation,
                         timestamp: Double) -> Bool {
        print("Witness status: \(self.status.rawValue) - startWitnessing")
        if status == .idle {
            self.error = nil
            self.proofOfLocationIDs = proofOfLocationIDs
            self.location = location
            self.timestamp = timestamp
            self.centralManager = CBCentralManager(delegate: self, queue: self.dispatchQueue)
            return true
        } else {
            return false
        }
    }

    ///
    /// Stops the scanning of BLE peripherals
    ///
    @objc private func stopScanForPeripherals() {
        print("Witness status: \(self.status.rawValue) - stopScan")
        self.status = .looping
        self.centralManager?.stopScan()
        self.blePeripheralsIndex = 0
        self.loopBLEPeripherals()
    }

    ///
    /// Loops the BLE peripherals found and tries to connect
    ///
    func loopBLEPeripherals() {
        print("Witness status: \(self.status.rawValue) - loopPeripherals")
        if (self.blePeripheralsIndex < self.blePeripherals!.count) {
            self.centralManager?.connect(self.blePeripherals![self.blePeripheralsIndex],
                                         options: nil)
            self.bleConnectionAttemptTimeoutTimer =
                Timer.scheduledTimer(timeInterval: DSGProofOfLocationExchangeConfiguration.bleConnectionAttemptTimeout,
                                     target: self,
                                     selector: #selector(stopAttemptToConnect),
                                     userInfo: nil,
                                     repeats: false)
        } else {
            self.delegate?.witness(didFailWith: DSGProofOfLocationExchangerError.POLProtocol.notFound,
                                   proofOfLocationIDs: self.proofOfLocationIDs!)
            self.centralManager = nil
            self.status = .idle
        }
    }

    ///
    /// Stops an attempt to connect to a BLE peripheral
    ///
    @objc func stopAttemptToConnect() {
        print("Witness status: \(self.status.rawValue) - stopAttemptToConnect")
        self.centralManager?.cancelPeripheralConnection(self.blePeripherals![self.blePeripheralsIndex])
    }

    ///
    /// Reads the proof of location ID from the BLE characteristic
    ///
    func readProofOfLocationID(peripheral: CBPeripheral) {
        print("Witness status: \(self.status.rawValue) - readProofOfLocationID")
        if let characteristicID = self.bleCharacteristicID {
            peripheral.readValue(for: characteristicID)
        } else {
            self.centralManager?.cancelPeripheralConnection(peripheral)
        }
    }
    
    ///
    /// Sends a randomly-generated string H to the prover
    ///
    func sendStringH(peripheral: CBPeripheral) {
        print("Witness status: \(self.status.rawValue) - sendStringH")
        self.stringH = DSGStringH()
        peripheral.writeValue(Data(self.stringH!.bytes), for: self.bleCharacteristicDataRx!, type: .withResponse)
    }
    
    ///
    /// Starts the distance-bounding process
    ///
    func startDistanceBounding(peripheral: CBPeripheral) {
        print("Witness status: \(self.status.rawValue) - startDistanceBounding")
        self.status = .distanceBounding
        peripheral.setNotifyValue(true, for: self.bleCharacteristicDBTx!)
        self.distanceBoundingStartTimestamps = []
        self.distanceBoundingEndTimestamps = []
        self.distanceBoundingIterationIndex = 0
        self.stringR = DSGStringR()
        self.stringC = DSGStringC()
        
        self.distanceBoundingIteration(peripheral: peripheral)
    }
    
    ///
    /// Performs a distance bounding iteration, sending a byte of the string C
    ///
    func distanceBoundingIteration(peripheral: CBPeripheral) {
        print("Witness status: \(self.status.rawValue) - distanceBoundingIteration")
        self.distanceBoundingStartTimestamps!.append(DispatchTime.now())
        peripheral.writeValue(Data([self.stringC!.bytes[self.distanceBoundingIterationIndex]]),
                              for: self.bleCharacteristicDBRx!,
                              type: .withoutResponse)
    }
    
    ///
    /// Checks if the distance bounding packets arrived in time
    ///
    func checkTimings(peripheral: CBPeripheral) {
        print("Witness status: \(self.status.rawValue) - checkTimings")
        var failed = 0
        for i in (0..<DSGStringC.size) {
            let startTimeInNanos = self.distanceBoundingStartTimestamps![Int(i)].uptimeNanoseconds
            let endTimeInNanos = self.distanceBoundingEndTimestamps![Int(i)].uptimeNanoseconds
            let rttInNanos = endTimeInNanos - startTimeInNanos
            let rttInMillis = rttInNanos / 1_000_000
            let maxDelay = DSGProofOfLocationExchangeConfiguration.bleOverheadTime +
                DSGProofOfLocationExchangeConfiguration.bleRoundTripTime
            if (rttInMillis > maxDelay) {
                failed += 1
            }
        }
        
        if (failed > DSGProofOfLocationExchangeConfiguration.maxDistanceBoundingLatePackets) {
            status = .finished
            let result = DSGProofOfLocationResult(isValid: false)
            peripheral.writeValue(Data(result.bytes),
                                  for: self.bleCharacteristicDataRx!,
                                  type: .withoutResponse)
            let _ = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { tim in
                self.centralManager?.cancelPeripheralConnection(peripheral)
            }
            self.delegate?.witness(didDiscard: self.proofOfLocationID!)
        } else {
            status = .finished
            let result = DSGProofOfLocationResult(isValid: true)
            peripheral.writeValue(Data(result.bytes),
                                  for: self.bleCharacteristicDataRx!,
                                  type: .withoutResponse)
            self.sendProofOfLocation(peripheral: peripheral)
        }
    }
    
    ///
    /// Sends a proof of location
    ///
    func sendProofOfLocation(peripheral: CBPeripheral) {
        print("Witness status: \(self.status.rawValue) - sendProofOfLocation")
        let proofOfLocation = DSGProofOfLocation(r: self.stringR!,
                                                 c: self.stringC!,
                                                 h: self.stringH!,
                                                 e: self.messageE!,
                                                 witnessID: self.witnessID,
                                                 location: self.location!,
                                                 timestamp: self.timestamp!)
        
        let pubk = try! PublicKey(base64Encoded: DSGProofOfLocationExchangeConfiguration.verifierPublicKey)
        let pvtk = try! PrivateKey(base64Encoded: self.signingKey)
        
        let clear = ClearMessage(data: Data(proofOfLocation.bytes))
        
        let signature = try! clear.signed(with: pvtk, digestType: .sha256)
        let signatureData = signature.data
        
        var signedPoL = proofOfLocation.bytes
        signedPoL.append(contentsOf: [UInt8](signatureData))
        
        let aes128KeyArr = (1...16).map( {_ in UInt8.random(in: 0...255)} )
        
        let encryptedData = DSGEncryption.aesEncryption(data:Data(signedPoL),
                                         keyData: Data(aes128KeyArr),
                                         operation:kCCEncrypt)
        
        let clearKey = ClearMessage(data: Data(aes128KeyArr))
        let encryptedKey = try! clearKey.encrypted(with: pubk, padding: .PKCS1)
        
        var mergedData = [UInt8](encryptedData)
        mergedData.append(contentsOf: [0x2A, 0x2A, 0x2A])
        mergedData.append(contentsOf: [UInt8](encryptedKey.data))
        mergedData.append(contentsOf: [0x2A, 0x2A, 0x2A])
        
//        let clear = ClearMessage(data: Data(proofOfLocation.bytes))
//
//        let signature = try! clear.signed(with: pvtk, digestType: .sha256)
//        let signatureData = signature.data
//
//        var signedProofOfLocation = proofOfLocation.bytes
//        signedProofOfLocation.append(contentsOf: [UInt8](signatureData))
//
//        let clearSigned = ClearMessage(data: Data(signedProofOfLocation))
//        let encryptedProofOfLocation = try! clearSigned.encrypted(with: pubk, padding: .PKCS1)
//        let encryptedProofOfLocationData = encryptedProofOfLocation.data
        var encryptedProofOfLocationDataBytesWithLenght = [UInt8(mergedData.count / 256), UInt8(mergedData.count % 256)]
        encryptedProofOfLocationDataBytesWithLenght.append(contentsOf: [UInt8](mergedData))
        
        DispatchQueue.global(qos: .userInitiated).async {
            let bytes = encryptedProofOfLocationDataBytesWithLenght
            let count = bytes.count
            let chunkSize = Int(DSGProofOfLocationExchangeConfiguration.maxBLEPacketPayloadSize)
            let numberOfChunks = ( Double(count) / Double(chunkSize) )
            let numberOfChunksRoundedUp = Int(numberOfChunks.rounded(.up))
            for chunkIndex in 0..<numberOfChunksRoundedUp {
                let lastIndex = min(count - 1, chunkIndex * chunkSize + (chunkSize-1) )
                let startIndex = chunkIndex * chunkSize
                let subBytes = Array(bytes[startIndex...lastIndex])
                let subData = Data(subBytes)
                Thread.sleep(forTimeInterval: 0.1)
                peripheral.writeValue(subData, for: self.bleCharacteristicDataRx!, type: .withoutResponse)
            }
            Thread.sleep(forTimeInterval: 0.1)
            self.centralManager?.cancelPeripheralConnection(peripheral)
            self.delegate?.witness(didSend: proofOfLocation, proofOfLocation: self.proofOfLocationID!)
        }
            
    }
    
}

//MARK: - CBCentralManagerDelegate

extension DSGWitness: CBCentralManagerDelegate {

    ///
    /// Checks the status of the BLE central manager
    ///
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("Witness status: \(self.status.rawValue) - centralManagerDidUpdateState")
        switch central.state {
        //In this case, the BLE is turned on and available
        case .poweredOn:
            self.status = .scanning
            self.blePeripherals = []
            self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
            self.bleScanTimeoutTimer =
                Timer.scheduledTimer(timeInterval: DSGProofOfLocationExchangeConfiguration.bleScanTimeout,
                                           target: self,
                                         selector: #selector(stopScanForPeripherals),
                                         userInfo: nil,
                                          repeats: false)
        //In every other case, the BLE is not available
        case .unknown:
            self.delegate?.witness(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                   proofOfLocationIDs: self.proofOfLocationIDs!)
            self.centralManager = nil
            self.status = .idle
        case .resetting:
            self.delegate?.witness(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                   proofOfLocationIDs: self.proofOfLocationIDs!)
            self.centralManager = nil
            self.status = .idle
        case .unsupported:
            self.delegate?.witness(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                   proofOfLocationIDs: self.proofOfLocationIDs!)
            self.centralManager = nil
            self.status = .idle
        case .unauthorized:
            self.delegate?.witness(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                   proofOfLocationIDs: self.proofOfLocationIDs!)
            self.centralManager = nil
            self.status = .idle
        case .poweredOff:
            self.delegate?.witness(didFailWith: DSGProofOfLocationExchangerError.BLE.unavailable,
                                   proofOfLocationIDs: self.proofOfLocationIDs!)
            self.centralManager = nil
            self.status = .idle
        @unknown default:
            fatalError()
        }
    }

    ///
    /// If the conditions are met, adds the discovered BLE peripherals to the list
    ///
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Witness status: \(self.status.rawValue) - centralManager:didDiscover")
        if ( Int32(truncating: RSSI) > DSGProofOfLocationExchangeConfiguration.minBLEPeripheralRSSI ) {
            if (!((self.blePeripherals?.contains(peripheral))!)) {
                self.blePeripherals!.append(peripheral)
            }
        }
    }

    ///
    /// The attempt to connect a peripheral failed
    ///
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Witness status: \(self.status.rawValue) - centralManager:didFailToConnect")
        self.bleConnectionAttemptTimeoutTimer?.invalidate()
        self.bleConnectionAttemptTimeoutTimer = nil
        self.blePeripheralsIndex += 1
        self.loopBLEPeripherals()
    }

    ///
    /// A BLE peripheral has been disconnected
    ///
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Witness status: \(self.status.rawValue) - centralManager:didDisconnectPeripheral")
        self.connectedBLEPeripheral = nil
        switch self.status {
        case .looping:
            self.bleConnectionAttemptTimeoutTimer?.invalidate()
            self.bleConnectionAttemptTimeoutTimer = nil
            self.error = nil
            self.blePeripheralsIndex += 1
            self.loopBLEPeripherals()
        case .finished:
//            self.centralManager = nil
            self.status = .idle
        default:
            if let error = self.error {
                self.delegate?.witness(didFailWith: error,
                                       proofOfLocationIDs: self.proofOfLocationIDs!)
            } else {
                self.delegate?.witness(didFailWith: DSGProofOfLocationExchangerError.BLE.disconnected,
                                       proofOfLocationIDs: self.proofOfLocationIDs!)
            }

            self.centralManager = nil
            self.status = .idle
        }
    }

    ///
    /// A BLE peripheral has been connected
    ///
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Witness status: \(self.status.rawValue) - centralManager:didConnect")
        self.bleConnectionAttemptTimeoutTimer?.invalidate()
        self.bleConnectionAttemptTimeoutTimer = nil
        self.connectedBLEPeripheral = peripheral
        peripheral.delegate = self
        peripheral.discoverServices([DSGBLEService.POLServiceUUID])
    }

}

//MARK: - CBPeripheralDelegate

extension DSGWitness: CBPeripheralDelegate {

    ///
    /// The services of the peripheral has been discovered
    ///
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("Witness status: \(self.status.rawValue) - peripheral:didDiscoverServices")
        guard let services = peripheral.services else {
            //If no Service is found, cancel the peripheral connection
            centralManager?.cancelPeripheralConnection(peripheral)
            return
        }
        var serviceFound = false
        for service in services {
            if service.uuid == DSGBLEService.POLServiceUUID {
                serviceFound = true
                peripheral.discoverCharacteristics([DSGBLEService.POLIDCharacteristicUUID,
                                                    DSGBLEService.DBRxCharacteristicUUID,
                                                    DSGBLEService.DBTxCharacteristicUUID,
                                                    DSGBLEService.DataTxCharacteristicUUID,
                                                    DSGBLEService.DataRxCharacteristicUUID],
                                                    for: service)
            }
        }
        if !serviceFound {
            //If the POLExchange Service is not found, cancel the peripheral connection
            centralManager?.cancelPeripheralConnection(peripheral)
        }
    }

    ///
    /// The characteristics of the service has been discovered
    ///
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("Witness status: \(self.status.rawValue) - peripheral:didDiscoverCharacteristicsFor")
        guard service.uuid == DSGBLEService.POLServiceUUID else {
            centralManager?.cancelPeripheralConnection(peripheral)
            return
        }

        guard let characteristics = service.characteristics else {
            centralManager?.cancelPeripheralConnection(peripheral)
            return
        }

        self.bleCharacteristicID = nil
        self.bleCharacteristicDataRx = nil
        self.bleCharacteristicDataTx = nil
        self.bleCharacteristicDBRx = nil
        self.bleCharacteristicDBTx = nil

        for characteristic in characteristics {
            if (characteristic.uuid == DSGBLEService.POLIDCharacteristicUUID) {
                self.bleCharacteristicID = characteristic
            } else if (characteristic.uuid == DSGBLEService.DataRxCharacteristicUUID) {
                self.bleCharacteristicDataRx = characteristic
            } else if (characteristic.uuid == DSGBLEService.DataTxCharacteristicUUID) {
                self.bleCharacteristicDataTx = characteristic
            } else if (characteristic.uuid == DSGBLEService.DBRxCharacteristicUUID) {
                self.bleCharacteristicDBRx = characteristic
            } else if (characteristic.uuid == DSGBLEService.DBTxCharacteristicUUID) {
                self.bleCharacteristicDBTx = characteristic
            }
        }

        guard self.bleCharacteristicID != nil,
              self.bleCharacteristicDataRx != nil,
              self.bleCharacteristicDataTx != nil,
              self.bleCharacteristicDBRx != nil,
              self.bleCharacteristicDBTx != nil else {
            centralManager?.cancelPeripheralConnection(peripheral)
            return
        }

        self.readProofOfLocationID(peripheral: peripheral)
    }

    ///
    /// A BLE characteristic was read or has changed
    ///
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Witness status: \(self.status.rawValue) - peripheral:didUpdateValueFor:characteristic")
        if status == .finished {
            return
        }
        
        if let value = characteristic.value {
            switch self.status {
            case .looping:
                //In this case, the witness has to check that the PoL id corresponds
                guard characteristic.uuid == self.bleCharacteristicID!.uuid else {
                    self.error = DSGProofOfLocationExchangerError.BLE.inconsistency
                    self.centralManager?.cancelPeripheralConnection(peripheral)
                    return
                }
                
                let receivedID = DSGProofOfLocationID(data: value)
                guard let safeReceivedID = receivedID else {
                    self.centralManager?.cancelPeripheralConnection(peripheral)
                    return
                }
                
                if (self.proofOfLocationIDs!.contains(safeReceivedID)) {
                    //Proof of location can be served
                    self.proofOfLocationID = receivedID
                    self.status = .matched
                    self.messageEBuffer = []
                    self.messageE = nil
                    peripheral.setNotifyValue(true, for: self.bleCharacteristicDataTx!)
                    return
                } else {
                    self.centralManager?.cancelPeripheralConnection(peripheral)
                    return
                }
            case .matched:
                //In this case, the witness has to receive the message e
                guard characteristic.uuid == self.bleCharacteristicDataTx!.uuid else {
                    self.error = DSGProofOfLocationExchangerError.BLE.inconsistency
                    self.centralManager?.cancelPeripheralConnection(peripheral)
                    return
                }
                if self.messageEncryptedSize == nil {
                    self.messageEncryptedSize = Int([UInt8](value)[0]) * 256 + Int([UInt8](value)[1])
                    self.messageEBuffer!.append(contentsOf: [UInt8](value)[2...])
                } else {
                    self.messageEBuffer!.append(contentsOf: [UInt8](value))
                }
                print("buffer: \(self.messageEBuffer!.count), len: \(self.messageEncryptedSize!)")
                if (self.messageEBuffer!.count == self.messageEncryptedSize!) {
                    self.messageE = DSGMessageE(bytes: self.messageEBuffer!)
                    if (self.messageE != nil) {
                        self.status = .messageEReceived
                        self.sendStringH(peripheral: peripheral)
                    } else {
                        self.error = DSGProofOfLocationExchangerError.BLE.message
                        self.centralManager?.cancelPeripheralConnection(peripheral)
                        return
                    }
                }
                
            
            case .distanceBounding:
                //In this case, the witness is waiting for the response of a distance bounding packet
                guard characteristic.uuid == self.bleCharacteristicDBTx!.uuid else {
                    self.error = DSGProofOfLocationExchangerError.BLE.inconsistency
                    self.centralManager?.cancelPeripheralConnection(peripheral)
                    return
                }
                self.distanceBoundingEndTimestamps!.append(DispatchTime.now())
                self.stringR!.append([UInt8](value).first!)
                
                if (self.stringR!.bytes.count < DSGStringR.size) {
                    self.distanceBoundingIterationIndex += 1
                    self.distanceBoundingIteration(peripheral: peripheral)
                } else {
                    self.status = .checking
                    self.checkTimings(peripheral: peripheral)
                }
                
                
            default:
                self.error = DSGProofOfLocationExchangerError.BLE.inconsistency
                self.centralManager?.cancelPeripheralConnection(peripheral)
            }
        } else {
            self.error = DSGProofOfLocationExchangerError.BLE.message
            self.centralManager?.cancelPeripheralConnection(peripheral)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Witness status: \(self.status.rawValue) - peripheral:didWriteValueFor:characteristic")
        if (self.status == .messageEReceived) {
            if (characteristic.uuid == self.bleCharacteristicDataRx!.uuid) {
                self.status = .stringHSent
                self.startDistanceBounding(peripheral: peripheral)
                return
            } else {
                self.error = DSGProofOfLocationExchangerError.BLE.inconsistency
                self.centralManager?.cancelPeripheralConnection(peripheral)
                return
            }
        }
    }
}
