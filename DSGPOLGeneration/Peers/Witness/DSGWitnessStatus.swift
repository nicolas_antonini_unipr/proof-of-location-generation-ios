//
//  DSGWitnessStatus.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 12/06/21.
//

import Foundation

///
/// Represent the possible status of a DSGWitness object
///
enum DSGWitnessStatus: String {
    
    ///
    /// In this status, the DSGWitness object has been initialized but is waiting
    /// to witness for a proof of location
    ///
    case idle = "Idle"
    
    ///
    /// In this status, the witness is scanning for BLE peripherals in order to find provers
    ///
    case scanning = "Scanning"
    
    ///
    /// In this status, the witness is looping the BLE peripherals it has found to find a suitable prover
    ///
    case looping = "Looping"
    
    ///
    /// In this status, the witness has found a suitable prover
    ///
    case matched = "Matched"
    
    ///
    /// In this status, the witness has received the message E from the prover
    ///
    case messageEReceived = "MessageEReceived"
    
    ///
    /// In this status, the witness has sent the string H
    ///
    case stringHSent = "StringHSent"
    
    ///
    /// In this status, the witness is performing the distance bounding protocol
    ///
    case distanceBounding = "DistanceBounding"
    
    ///
    /// In this status, the witness has received all the distance bounding packets and it's valuating the result
    ///
    case checking = "Checkin"
    
    ///
    /// In this status, the witness has valuated the results of the distance bounding protocol
    ///
    case finished = "Finished"
    
}
