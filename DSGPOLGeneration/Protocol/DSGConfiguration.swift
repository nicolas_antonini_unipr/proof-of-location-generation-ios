//
//  DSGProofOfLocationConfiguration.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

///
/// A class that contains the configuration of the Proof Of Location Exchange protocol
///
/// The class DSGProofOfLocationExchangeConfiguration contains a set of constants
/// on which the Proof Of Location Exchange protocol relies.
///
class DSGProofOfLocationExchangeConfiguration {
    
    ///
    /// The size (in bytes) of the strings A, B, C, H, R
    ///
    static var stringsSize: UInt32 = 8
    
    ///
    /// The size (in bytes) of a peer's ID (from 1 to maxBLEPacketPayloadSize)
    ///
    static var peerIDSize: UInt32 = 16
    
    ///
    /// The size (in bytes) of a proof of location id (from 1 to maxBLEPacketPayloadSize)
    ///
    static var proofOfLocationIDSize: UInt32 = 20

    ///
    /// The overhead time (in milliseconds) of the BLE communication technology
    ///
    static var bleOverheadTime: UInt32 = 60
    
    ///
    /// The RTT (round trip time) of the communication (in milliseconds)
    ///
    static var bleRoundTripTime: UInt32 = 30
    
    ///
    /// The maximum number of BLE packets that can arrive late before considering
    /// the proof of location invalid
    ///
    static var maxDistanceBoundingLatePackets: UInt32 = 2
    
    ///
    /// The minumum RSSI value (in dBm) before considering a BLE peripheral unreachable
    ///
    static var minBLEPeripheralRSSI: Int32 = -200
    
    ///
    /// The maximum size (in bytes) that a BLE packet payload can carry
    ///
    static var maxBLEPacketPayloadSize: UInt32 = 20
    
    ///
    /// The timeout (in seconds) of the BLE scan that searches for provers
    ///
    static var bleScanTimeout: Double = 3.0
    
    ///
    /// The timeout (in seconds) of an attempt to connect to a BLE peripheral
    ///
    static var bleConnectionAttemptTimeout: Double = 3.0
    
    ///
    /// The public key of the verifier
    ///
    static var verifierPublicKey: String = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJR6cZ6GxnnZW0zQsHkHmE1UQo6uMbSCkoGCh3Cr9o3X+51wE2mBQVT7zDcRorAT8ry1rSP2FrKsOq4bpIHEgRECAwEAAQ=="
}
