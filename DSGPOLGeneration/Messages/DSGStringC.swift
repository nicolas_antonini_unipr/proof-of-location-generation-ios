//
//  DSGStringC.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

class DSGStringC {
    
    ///
    /// The size of the string, in bytes.
    ///
    static let size: UInt32 = DSGProofOfLocationExchangeConfiguration.stringsSize
    
    private var stringCBytes: [UInt8]
    
    ///
    /// Class constructor. Initializes a random string C.
    ///
    init() {
        self.stringCBytes = []
        for _ in (0..<DSGProofOfLocationExchangeConfiguration.stringsSize) {
            self.stringCBytes.append(UInt8.random(in: 0...255))
        }
    }
    
    ///
    /// Gets the bytes representing the string C.
    ///
    var bytes: [UInt8] {
        return stringCBytes
    }
    
    ///
    /// Gets the bytes representing the string C.
    ///
    var data: Data {
        return Data(stringCBytes)
    }
}
