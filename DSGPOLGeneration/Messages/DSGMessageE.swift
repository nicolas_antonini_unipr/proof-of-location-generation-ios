//
//  DSGMessageE.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

class DSGMessageE {
    ///
    /// The size of the message, in bytes.
    ///
    static var size: UInt32 {
        return ((DSGProofOfLocationExchangeConfiguration.stringsSize * 2) +
            DSGProofOfLocationExchangeConfiguration.peerIDSize +
            DSGLocation.size)
    }
    
    private var messageBytes: [UInt8]
    
    
    ///
    /// Class constructor
    ///
    /// - parameter a: the string A
    /// - parameter b: the string B
    /// - parameter id: the ID of the prover
    /// - parameter location: the location of the prover
    ///
    init(a: DSGStringA,
         b: DSGStringB,
         id: DSGPeerID,
         location: DSGLocation) {
        var bytes: [UInt8] = []
        bytes.append(contentsOf: a.bytes)
        bytes.append(contentsOf: b.bytes)
        bytes.append(contentsOf: id.bytes)
        bytes.append(contentsOf: location.bytes)
        self.messageBytes = bytes
    }
    
    init?(bytes: [UInt8]) {
        self.messageBytes = bytes
    }
    
    var bytes: [UInt8] {
        return messageBytes
    }
}
