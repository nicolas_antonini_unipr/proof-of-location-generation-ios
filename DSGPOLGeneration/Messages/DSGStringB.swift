//
//  DSGStringB.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

class DSGStringB {
    
    ///
    /// The size of the string, in bytes.
    ///
    static let size: UInt32 = DSGProofOfLocationExchangeConfiguration.stringsSize
    
    private var stringBBytes: [UInt8]
    
    ///
    /// Class constructor. Initializes a random string B.
    ///
    init() {
        self.stringBBytes = []
        for _ in (0..<DSGProofOfLocationExchangeConfiguration.stringsSize) {
            self.stringBBytes.append(UInt8.random(in: 0...255))
        }
    }
    
    ///
    /// Gets the bytes representing the string B.
    ///
    var bytes: [UInt8] {
        return stringBBytes
    }
    
    ///
    /// Gets the data representing the string B.
    ///
    var data: Data {
        return Data(stringBBytes)
    }
}
