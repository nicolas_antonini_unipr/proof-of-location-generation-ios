//
//  DSGStringH.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

class DSGStringZ {
    
    ///
    /// The size of the string, in bytes.
    ///
    static let size: UInt32 = DSGProofOfLocationExchangeConfiguration.stringsSize
    
    private var stringZBytes: [UInt8]
    
    ///
    /// Class constructor. Creates the string Z starting from the string B and H
    ///
    init(b: DSGStringB, h: DSGStringH) {
        self.stringZBytes = []
        for i in 0..<(b.bytes.count) {
            self.stringZBytes.append(b.bytes[i] ^ h.bytes[i])
        }
    }
    
    ///
    /// Gets the bytes representing the string Z.
    ///
    var bytes: [UInt8] {
        return stringZBytes
    }
    
    ///
    /// Gets the data representing the string Z.
    ///
    var data: Data {
        return Data(stringZBytes)
    }
    
}
