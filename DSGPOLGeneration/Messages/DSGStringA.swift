//
//  DSGStringA.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

class DSGStringA {
    
    ///
    /// The size of the string, in bytes.
    ///
    static let size: UInt32 = DSGProofOfLocationExchangeConfiguration.stringsSize
    
    private var stringABytes: [UInt8]
    
    ///
    /// Class constructor. Initializes a random string A.
    ///
    init() {
        self.stringABytes = []
        for _ in (0..<DSGProofOfLocationExchangeConfiguration.stringsSize) {
            self.stringABytes.append(UInt8.random(in: 0...255))
        }
    }
    
    ///
    /// Gets the bytes representing the string A.
    ///
    var bytes: [UInt8] {
        return stringABytes
    }
    
    ///
    /// Gets the data representing the string A.
    ///
    var data: Data {
        return Data(stringABytes)
    }
}
