//
//  DSGStringH.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

class DSGStringH {
    
    ///
    /// The size of the string, in bytes.
    ///
    static let size: UInt32 = DSGProofOfLocationExchangeConfiguration.stringsSize
    
    private var stringHBytes: [UInt8]
    
    ///
    /// Class constructor. Initializes a random string H.
    ///
    init() {
        self.stringHBytes = []
        for _ in (0..<DSGProofOfLocationExchangeConfiguration.stringsSize) {
            self.stringHBytes.append(UInt8.random(in: 0...255))
        }
    }
    
    ///
    /// Class constructor.
    ///
    /// - parameter data: The Data representing the string H
    ///
    init?(data: Data) {
        let bytes = [UInt8](data)
        if bytes.count == DSGStringH.size {
            self.stringHBytes = bytes
        } else {
            return nil
        }
    }
    
    ///
    /// Gets the bytes representing the string H.
    ///
    var bytes: [UInt8] {
        return stringHBytes
    }
    
    ///
    /// Gets the data representing the string H.
    ///
    var data: Data {
        return Data(stringHBytes)
    }
    
}
