//
//  DSGStringR.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

class DSGStringR {
    
    ///
    /// The size of the string, in bytes.
    ///
    static let size: UInt32 = DSGProofOfLocationExchangeConfiguration.stringsSize
    
    private var stringRBytes: [UInt8]
    
    
    ///
    /// Class constructor.
    ///
    init() {
        self.stringRBytes = []
    }
    
    ///
    /// Appends a new element to the string.
    ///
    func append(_ newElement: UInt8) {
        self.stringRBytes.append(newElement)
    }
    
    
    var bytes: [UInt8] {
        return self.stringRBytes
    }
    
}
