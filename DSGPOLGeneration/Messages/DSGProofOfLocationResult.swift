//
//  DSGProofOfLocationResultMessage.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 12/06/21.
//

import Foundation


///
/// Represents the message that communicates if a proof of location is valid.
///
class DSGProofOfLocationResult {
    private(set) var bytes: [UInt8]
    
    private var successBytes: [UInt8] = [1]
    private var failedBytes: [UInt8] = [0]
    
    
    ///
    /// Class constructor.
    ///
    /// - parameter isValid: True if the proof of location is valid, else false
    ///
    init(isValid: Bool) {
        if isValid {
            self.bytes = successBytes
        } else {
            self.bytes = failedBytes
        }
    }
    
    ///
    /// Class constructor.
    ///
    /// - parameter data: The data representing the message
    ///
    init(data: Data) {
        self.bytes = [UInt8](data)
    }
    
    ///
    /// Returns true if the message tells that the proof of location is valid, else false.
    ///
    var isValid: Bool {
        var isValid = true
        for i in (0..<bytes.count) {
            if (bytes[i] != successBytes[i]) {
                isValid = false
            }
        }
        return isValid
    }
}
