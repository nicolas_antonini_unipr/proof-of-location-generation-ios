//
//  DSGProofOfLocationError.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation


///
/// The errors raised from the Proof of Location Exchanger Library
///
enum DSGProofOfLocationExchangerError {
    
    ///
    /// An error reguarding the BLE communication
    ///
    enum BLE: DSGError {
        
        ///The BLE is not currently available
        case unavailable
        
        ///The BLE peripheral disconnected unexpectedly
        case disconnected
        
        ///The occourred event and the current status are inconsistent
        case inconsistency
        
        ///The received message is not valid
        case message
        
        ///
        ///The univoque code of the error
        ///
        var code: Int {
            switch self {
            case .unavailable:
                return 100
            case .disconnected:
                return 200
            case .inconsistency:
                return 300
            case .message:
                return 400
            }
        }
        
        ///
        ///An human-readable string representing the error
        ///
        var errorDescription: String {
            switch self {
            case .unavailable:
                return "Bluetooth is not available"
            case .disconnected:
                return "Bluetooth Device has been unexpectedly disconnected"
            case .inconsistency:
                return "The current event is not consistent with the state"
            case .message:
                return "The received message is not valid"
            }
        }
        
    }
    
    ///
    /// An error reguarding the Proof Of Location Exchange Protocol
    ///
    enum POLProtocol: DSGError {
        
        ///No prover has been found
        case notFound
        
        ///
        ///The univoque code of the error
        ///
        var code: Int {
            switch self {
            case .notFound:
                return 010
            }
        }
        
        ///
        ///An human-readable string representing the error
        ///
        var errorDescription: String {
            switch self {
            case .notFound:
                return "No suitable prover has been found"
            }
        }
        
    }
}
