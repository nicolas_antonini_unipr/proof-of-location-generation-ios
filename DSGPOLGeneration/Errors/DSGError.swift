//
//  DSGError.swift
//  DSGProofOfLocationExchanger
//
//  Created by Nicolas Antonini on 01/06/21.
//

import Foundation

protocol DSGError: Error {
    var code: Int { get }
    var errorDescription: String { get }
}
